import numpy as np
import math
import imageTools
import gaussianBlur

#not actually booleans just 0 or 1
def generate_boolean_matrix(array, detail):
    height = array.shape[0]
    width = array.shape[1]
    boolean_matrix = np.zeros((height, width))
    for r in range (height):
        for c in range (width - 1):
            r_dif = array[r][c][0] - array[r][c + 1][0]
            g_dif = array[r][c][1] - array[r][c + 1][1]
            b_dif = array[r][c][2] - array[r][c + 1][2]

            if (r_dif * r_dif + g_dif * g_dif + b_dif * b_dif) > detail:
                boolean_matrix[r][c] = 1

    for c in range(width):
        for r in range(height - 1):
            r_dif = array[r][c][0] - array[r + 1][c][0]
            g_dif = array[r][c][1] - array[r + 1][c][1]
            b_dif = array[r][c][2] - array[r + 1][c][2]

            if (r_dif * r_dif + g_dif * g_dif + b_dif * b_dif) > detail:
                boolean_matrix[r][c] = 1

    return boolean_matrix



#should be black everywhere but blue where an edge is
def generate_edge_image(array, detail):
    height = array.shape[0]
    width = array.shape[1]
    edges = np.zeros((height, width, 3))
    for r in range(height):
        for c in range(width - 1):
            r_dif = int(array[r][c][0]) - int(array[r][c + 1][0])
            g_dif = int(array[r][c][1]) - int(array[r][c + 1][1])
            b_dif = int(array[r][c][2]) - int(array[r][c + 1][2])

            if (r_dif * r_dif + g_dif * g_dif + b_dif * b_dif) > detail:
                edges[r][c][2] = 255

    for c in range(width):
        for r in range(height - 1):
            r_dif = int(array[r][c][0]) - int(array[r + 1][c][0])
            g_dif = int(array[r][c][1]) - int(array[r + 1][c][1])
            b_dif = int(array[r][c][2]) - int(array[r + 1][c][2])

            if (r_dif * r_dif + g_dif * g_dif + b_dif * b_dif) > detail:
                edges[r][c][2] = 255

    return edges




