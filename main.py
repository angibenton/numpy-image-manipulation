import gaussianBlur as gb
import imageDisjoint as id
import imageTools
import numpy as np
from PIL import Image
import math
import edgeDetect
import time

start_time = time.time()

image = Image.open("images/smol.png")

original_array = np.array(image, dtype=np.uint8)

print("image width: ", len(original_array[0]))
print("image height: ", len(original_array))

# blurred_array = gb.blur(original_array, 1)

# edges_array = edgeDetect.generate_edge_image(blurred_array, 600)
# blurred_cat_image = Image.fromarray(edges_array.astype(np.uint8))

# blurred_cat_image.save("images/edges_cat.png")

#
# forrest_image = Image.open("images/forrest.jpeg")
# forrest_array = np.array(forrest_image, dtype=np.uint8)
# forrest_edges = edgeDetect.generate_edge_image(forrest_array, 200)
# forrest_edges_image = Image.fromarray(forrest_edges.astype(np.uint8))
# forrest_edges_image.save("images/forrest_edges.jpeg")

delta = 20
color_threshold = 50

# union-find data structure for the image
pixel_set = id.ImageDisjoint(original_array)

# map for the image (pixel_group : average color)
color_map = {}

for r in range(len(original_array)):
    cur_group = (r, 0)
    cur_avg = original_array[r][0]

    for c in range(len(original_array[0])):
        # this block is if the color IS similar
        if imageTools.color_comp(original_array[r][c], cur_avg, color_threshold):
            # change the average but weight the new value lower depending on how many
            # are in the row section so far
            weight = pixel_set.weight(cur_group[0], cur_group[1])
            cur_avg = ((original_array[r][c] + cur_avg * weight) / (weight + 1)).astype(int)
            pixel_set.union(cur_group[0], cur_group[1], r, c)
        else:
            # This block is if the color is NOT similar:
            # seal off the current group and its average color in the map
            color_map[cur_group] = cur_avg
            # start new group
            cur_group = (r, c)
            cur_avg = original_array[r][c]

for group in color_map.keys():
    print("this chunk: ", group[0], group[1])
    chunk = pixel_set.get_set(group[0], group[1])
    color_avg = color_map[group]
    for coord in chunk:
        print("coordinates: ", coord[0], coord[1])
        original_array[coord[0]][coord[1]][0] = color_avg[0]  # red
        original_array[coord[0]][coord[1]][1] = color_avg[1]  # green
        original_array[coord[0]][coord[1]][2] = color_avg[2]  # blue

image1 = Image.fromarray(original_array.astype(np.uint8))
image1.save("images/smol_test.jpeg")

end_time = time.time()
time = end_time - start_time
print("PROGRAM RUNTIME: %s SECONDS" % time)
