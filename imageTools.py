import numpy as np

# make sure an rgb array is safe
def color_fix(color):
    r = color[0]
    g = color[1]
    b = color[2]
    r = int(r)
    g = int(g)
    b = int(b)
    if r < 0: r = 0
    if g < 0: g = 0
    if b < 0: b = 0
    if r > 255: r = 255
    if g > 255: g = 255
    if b > 255: b = 255

    fixed = [r, g, b]
    return fixed


'''
       This functions takes a ppm image stored in the format of a 3D numpy list [y][x][c] where
       c is color channel
           0 for red
           1 for green
           2 for blue
       and writes a plain text P3 style PPM image under the name filename
       Args: image name, image data
       DOES NOT WORK 
       Return: none
       '''


def write_ppm(filename, image):
    height = image.shape[0]
    width = image.shape[1]
    print("writing image with dimensions", height, " X ", width)
    file = open(filename, "w")
    file.write("P3\n")
    file.write(str(width) + " " + str(height) + "\n")
    percent_done = 0
    for y in range(height):
        if (y / height * 100 > percent_done):
            percent_done = y / height * 100
        for x in range(width):
            for c in range(3):
                file.write(str(int(image[y][x][c])) + " ")
            file.write("\n")
    file.close()


# return a black and white rep of matrix
# for looking at my gaussian matrix (debugging)
def visualize_2d_matrix(matrix):
    image = np.zeros((matrix.shape[0], matrix.shape[1], 3))
    for r in range(matrix.shape[0]):
        for c in range(matrix.shape[1]):
            value = 1000000 * matrix[r][c]
            color = color_fix([value, value, value])
            assert isinstance(image, np.ndarray)
            image[r][c][0] = color[0]
            image[r][c][1] = color[1]
            image[r][c][2] = color[2]

    return image


#return true if two pixels are very close in total color
def color_comp(color1, color2, threshold):
    diff = np.sum(np.square(color1 - color2))
    if diff > threshold:
        return False
    return True


