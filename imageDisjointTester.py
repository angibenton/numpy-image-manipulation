import unittest
import imageDisjoint
import numpy as np


class MyTestCase(unittest.TestCase):
    img = np.zeros((2, 2, 3))
    set = imageDisjoint.ImageDisjoint(img)

    def test_something(self):
        print("test_something")
        self.assertEqual(True, True)

    def test_unionThenFind(self):
        print("test_unionThenFind")
        #img = np.zeros((2, 2, 3))
        #set = imageDisjoint.ImageDisjoint(self.img)

        # two points that should not be unioned right now
        self.assertFalse(self.set.find(0, 0) == self.set.find(0, 1))
        self.set.union(0, 0, 0, 1)

        # now they should be unioned
        self.assertTrue(self.set.find(0, 0) == self.set.find(0, 1))

    def test_weightStartsAt1(self):
        print("test_weightStartsAt1")
        self.assertEqual(self.set.weight(0, 0), 1)
        self.assertEqual(self.set.weight(0, 1), 1)
        self.assertEqual(self.set.weight(1, 0), 1)
        self.assertEqual(self.set.weight(1, 1), 1)




if __name__ == '__main__':
    unittest.main()
