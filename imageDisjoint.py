import numpy as np

#union find data structure specifically for pixels

class ImageDisjoint(object):
    universe = []
    weights = []
    width = 0
    height = 0
    size = 0

    def __init__(self, array):
        self.height = len(array)
        self.width = len(array[0])
        self.size = self.width * self.height
        # make a 1d array to represent all the pixels
        self.universe = np.empty(self.size)
        self.weights = np.ones(self.size)
        for i in range(self.size):
            self.universe[i] = i

    def find(self, r, c):
        '''
        :param r:
        :param c:
        :return:
        '''
        return self.recursive_find(int(r * self.width + c))

    def recursive_find(self, pixel_num):
        if self.universe[int(pixel_num)] == int(pixel_num):
            return int(pixel_num)
        parent = int(self.universe[pixel_num])
        root = self.recursive_find(parent)
        self.universe[pixel_num] = root  # path compression
        return root

    def union(self, r1, c1, r2, c2):
        root1 = self.find(r1, c1)
        root2 = self.find(r2, c2)
        if root1 == root2:
            return
        if self.weights[root1] > self.weights[root2]: #first set is bigger, union 2 to 1
            self.universe[root2] = root1
            self.weights[root1] += 1
        else: #second set is bigger, union 1 to 2
            self.universe[root1] = root2
            self.weights[root2] += 1

    def weight(self, r, c):
        num = int(r * self.width + c)
        return self.weights[num]

    #return an array of tuples that are unioned with the parameter pixel
    def get_set(self, r, c):
        local_set = []
        root_pixel = self.recursive_find(r * self.width + c)
        for i in range(self.size):
            cur_root = self.recursive_find(i)
            if cur_root == root_pixel:
                row = int(i / self.height)
                col = i - (row * self.height)
                local_set.append((row, col))
        return local_set





