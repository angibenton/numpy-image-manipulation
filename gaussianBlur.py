from PIL import Image
import numpy as np
import math
import imageTools


# make a convolution matrix with size about 10 x sigma + 1
def convolution_matrix(sigma):
    size = math.ceil(10 * sigma)
    if (size % 2 == 0):
        size += 1  # make sure it is odd size so that there is a "middle"
    middle = size / 2
    matrix = np.zeros((size, size))
    for r in range(size):  # go thru rows
        for c in range(size):  # go thru columns
            dx = c - middle
            dy = r - middle
            # with increasing dy and dx (distance from center), g, the value at r,c decreases exponentially
            g = ((1 / math.sqrt(2.0 * math.pi * sigma * sigma)) * math.exp(-(dx * dx + dy * dy) / (2 * sigma * sigma)))
            matrix[r][c] = g
            print(g)
        print("\n")
    print("got convolution matrix \n")
    return matrix


# old_im  = numpy array, row and col = int
# return a color tuple for that pixel

def get_blur_pixel(old_im, row, col, gaussian):
    gauss_size = gaussian.shape[0]
    mid = gauss_size / 2
    height = old_im.shape[0]
    width = old_im.shape[1]
    red_sum = 0
    green_sum = 0
    blue_sum = 0
    gauss_sum = 0

    # The pixel given by row, col should map to the center of the gaussian,
    # so the other pixels line up following that and get their rgb values multiplied by
    # the corresponding gaussian value
    # and then added to the rgb sums
    for g_row in range(gaussian.shape[0]):  # go thru rows OF GAUSSIAN
        im_y = int(g_row - mid + row)  # get the y index for the image
        for g_col in range(gaussian.shape[1]):  # go thru cols OF GAUSSIAN
            im_x = int(g_col - mid + col)  # get the x index for the image
            if 0 <= im_x < width and 0 <= im_y < height:  # didn't fall off image edge
                red_sum += old_im[im_y][im_x][0] * gaussian[g_row][g_col]
                green_sum += old_im[im_y][im_x][1] * gaussian[g_row][g_col]
                blue_sum += old_im[im_y][im_x][2] * gaussian[g_row][g_col]
                gauss_sum += gaussian[g_row][g_col]

    # could be non-int, maybe outside of [0,255] ?
    raw_color = [red_sum / gauss_sum, green_sum / gauss_sum, blue_sum / gauss_sum]

    # fix it
    return imageTools.color_fix(raw_color)


# takes a numpy array representation of image and returns a blurred one
# sigma = extent of blur
def blur(im, sigma):
    height = im.shape[0]
    width = im.shape[1]
    new_im = np.zeros((height, width, 3))
    gaussian = convolution_matrix(sigma)
    for r in range(height):
        print("blurring row #: ", end="")
        print(r)
        for c in range(width):
            new_im[r][c] = get_blur_pixel(im, r, c, gaussian)
    return new_im



